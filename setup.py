from pathlib import Path
from setuptools import setup, find_packages


def get_version():
    path = Path("VERSION")
    if path.is_file():
        return path.read_text()
    return "0.0.0"


PKG_NAME = "autovlab-cli"
setup(
    name=PKG_NAME,
    version=get_version(),
    packages=find_packages(),
    entry_points={
        "console_scripts": [f"{PKG_NAME}=autovlab_cli.main:app"],
    },
    install_requires=[
        "typer",
        "colorama",
        "tabulate",
    ],
)
