import secrets
from typing import Optional, List

import typer

from autovlab_cli import app, utils


@app.command()
def check_login_status():
    return utils.cli_call(app.autovlab.auth_api.check_login_status)


@app.command()
def refresh_token():
    return utils.cli_call(app.autovlab.auth_api.refresh_token)


@app.command()
def logout():
    res = utils.cli_call(app.autovlab.auth_api.logout)
    app.autovlab.config.access_token = None
    return res
