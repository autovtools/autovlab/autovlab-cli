import secrets
from typing import Optional, List

import typer

from autovlab_api import (
    UserCreateSpec,
    GroupCreateSpec,
    RoleCreateSpec,
    UserUpdateSpec,
    GroupUpdateSpec,
    RoleUpdateSpec,
)

from autovlab_cli import app, utils


@app.command()
def create_user(
    name: str = typer.Argument(..., help="Username for the user"),
    locked: bool = typer.Option(
        False, "--lock", "-l", help="Lock the account after creation"
    ),
    roles: List[str] = typer.Option(
        None, "--roles", "-r", help="Role to assign to this user (repeatable)"
    ),
    groups: List[str] = typer.Option(
        None, "--group", "-g", help="Group to assign to this user (repeatable)"
    ),
):
    password = utils.gen_password()
    typer.echo(f"Generated pasword: {password}")
    spec = UserCreateSpec(
        name=name, password=password, locked=locked, roles=roles, groups=groups
    )
    return utils.cli_call(app.autovlab.admin_api.create_user, spec)


@app.command()
def create_group(
    name: str = typer.Argument(..., help="Name for the group"),
    protected: bool = typer.Option(
        False, "--protected", "-p", help="Protect the group and all projects within it"
    ),
    users: List[str] = typer.Option(
        None, "--user", "-u", help="User to add to this group (repeatable)"
    ),
):
    spec = GroupCreateSpec(
        name=name,
        protected=protected,
        users=users,
    )
    return utils.cli_call(app.autovlab.admin_api.create_group, spec)


@app.command()
def create_role(
    name: str = typer.Argument(..., help="Name for the role"),
    users: List[str] = typer.Option(
        None, "--user", "-u", help="User to add to this role (repeatable)"
    ),
):
    spec = RoleCreateSpec(
        name=name,
        users=users,
    )
    return utils.cli_call(app.autovlab.admin_api.create_role, spec)


@app.command()
def get_users():
    return utils.cli_call(app.autovlab.admin_api.get_users)


@app.command()
def get_groups():
    return utils.cli_call(app.autovlab.admin_api.get_groups)


@app.command()
def get_roles():
    return utils.cli_call(app.autovlab.admin_api.get_roles)


@app.command()
def delete_user(
    name: str = typer.Argument(..., help="Name of the user to delete"),
):
    users = app.autovlab.admin_api.get_users()
    user = utils.get_by_name(users, name)
    if user is None:
        typer.echo(f"No user named {name}")
        raise typer.Exit(1)
    elif not hasattr(user, "id"):
        typer.echo(user)
        raise typer.Exit(1)

    return utils.cli_call(app.autovlab.admin_api.delete_user, user.id)


@app.command()
def delete_group(
    name: str = typer.Argument(..., help="Name of the group to delete"),
):
    groups = app.autovlab.admin_api.get_groups()
    group = utils.get_by_name(groups, name)
    if group is None:
        typer.echo(f"No group named {name}")
        raise typer.Exit(1)
    elif not hasattr(group, "id"):
        typer.echo(group)
        raise typer.Exit(1)

    return utils.cli_call(app.autovlab.admin_api.delete_group, group.id)


@app.command()
def delete_role(
    name: str = typer.Argument(..., help="Name of the role to delete"),
):
    roles = app.autovlab.admin_api.get_roles()
    role = utils.get_by_name(roles, name)
    if role is None:
        typer.echo(f"No role named {name}")
        raise typer.Exit(1)
    elif not hasattr(role, "id"):
        typer.echo(role)
        raise typer.Exit(1)

    return utils.cli_call(app.autovlab.admin_api.delete_role, role.id)
