from collections.abc import Iterable

from tabulate import tabulate

# https://pypi.org/project/tabulate/
DEFAULT_TABLE_FMT = "grid"


def from_obj_list(objs):
    objs = [d.to_dict() if hasattr(d, "to_dict") else d for d in objs]

    tbl = []
    headers = []
    if objs and isinstance(objs[0], dict):
        headers = list(objs[0])
        for obj in objs:
            row = []
            if isinstance(obj, dict):
                row = [
                    table(val)
                    if isinstance(val, list) or isinstance(val, dict)
                    else val
                    for val in obj.values()
                ]
            tbl.append(row)
    else:
        headers = []
        tbl = "\n".join(objs)

    return headers, tbl


def table(tbl, headers=None, tablefmt=DEFAULT_TABLE_FMT):
    if not tbl:
        return ""
    if hasattr(tbl, "to_dict"):
        tbl = tbl.to_dict()
    if isinstance(tbl, list):
        _headers, tbl = from_obj_list(tbl)
        if headers is None:
            headers = _headers
    elif isinstance(tbl, dict):
        # The default dict formatting is not very useful
        # Basically, make a pretty form of json.dumps() for dicts
        if headers is None:
            headers = ["Key", "Value"]
        rows = []
        for key, val in tbl.items():
            # Format nested dicts as tables
            if isinstance(val, dict):
                val = table(val)
            elif isinstance(val, list):
                val = table(val)
            rows.append([key, val])
        tbl = rows
    if isinstance(tbl, str):
        return tbl
    return tabulate(tbl, headers=headers, tablefmt=tablefmt)
