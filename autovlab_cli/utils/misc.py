import secrets

import typer
from .fmt import table


def gen_password(num_bytes=64):
    return secrets.token_hex(num_bytes)


def cli_call(_func, *args, **kwargs):
    try:
        res = _func(*args, **kwargs)
        typer.echo("Result:")
        typer.echo(table(res))
        return res
    except Exception as e:
        typer.echo(f"Error: {e}")
        raise typer.Exit(code=1)


def get_by_name(items, name):
    print(f"Searching for {name}")
    for item in items:
        if isinstance(item, dict):
            if item.get("name", None) == name:
                return item
        elif hasattr(item, "name"):
            if item.name == name:
                return item
    return None
