import os
from pathlib import Path

from autovlab_api import Configuration

from .env import get_secret


def load_config():
    config = Configuration()
    config.host = "https://autovlab:8842"
    config.username = get_secret("AUTOVLAB_USER")
    config.password = get_secret("AUTOVLAB_PASSWORD")

    config.verify_ssl = True
    config.debug = False

    config.ssl_ca_cert = find_ca_bundle()
    return config


def find_ca_bundle():
    if "AUTOVLAB_CA_CRT" in os.environ:
        path = os.environ["AUTOVLAB_CA_CRT"]
        return path

    for path in ["/etc/ssl/certs/ca-certificates.crt", "/etc/ssl/certs/ca-bundle.crt"]:
        if Path(path).is_file():
            print(f"Using system ca bundle: {path}")
            return str(path)
    return None
