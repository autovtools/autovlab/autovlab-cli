from typing import Optional, List

import typer

from autovlab_cli import app, utils


@app.command()
def echo(msg: str):
    typer.echo(f"Echo: {msg}")
    tbl = [
        ["A", "B", "C"],
        ["1", "2", "3"],
    ]
    typer.echo(utils.table(tbl, headers=["Col1", "Col2", "Col3"]))
    d = {
        "SomeKey": "SomeVal",
        "SomeOtherKey": "SomeOtherVal",
        "NestedDict": {
            "NestedKey": "NestedVal",
        },
    }
    typer.echo(utils.table(d))


@app.command(help="Interactive (raw Python) shell.")
def shell():
    try:
        import IPython
    except ImportError as e:
        typer.echo(f"shell requires IPython package to be installed")
    else:
        IPython.embed()
