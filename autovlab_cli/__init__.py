import typer

from autovlab_api import ApiClient, AuthApi, AdminApi

from . import utils


class AutovLab:
    def __init__(self, config):
        self.config = config
        self.client = ApiClient(self.config)
        # APIs
        self.auth_api = AuthApi(self.client)
        self.admin_api = AdminApi(self.client)

    def login(self):
        token = self.auth_api.login(
            username=self.config.username, password=self.config.password
        )
        self.config.access_token = token.access_token


def init():
    if not hasattr(app, "autovlab"):
        autovlab = AutovLab(utils.load_config())
        autovlab.login()
        setattr(app, "autovlab", autovlab)


app = typer.Typer(callback=init)

from .cmd import auth, admin
