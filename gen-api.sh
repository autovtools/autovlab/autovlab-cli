#!/bin/bash

set -e


OPENAPI_URL="https://autovlab:8842/openapi.json"
OUTDIR="autovlab-api"

rm -f openapi.json*
rm -rf ./__pycache__
rm -rf "${OUTDIR}"

wget --no-check-certificate "${OPENAPI_URL}"

mkdir -p "${OUTDIR}"
sudo chcon -Rt svirt_sandbox_file_t "${OUTDIR}"

# swagger_to_py_client.py --force --swagger_path ./swagger.json --outpath client.py
docker run -it --rm \
    -v "$(pwd)/codegen.json:/codegen.json" \
    -v "$(pwd)/openapi.json:/openapi.json" \
    -v "$(pwd)/${OUTDIR}:/tmp/${OUTDIR}"\
    openapitools/openapi-generator-cli \
    generate \
    -c /codegen.json \
    -i /openapi.json \
    -g python \
    -o "/tmp/${OUTDIR}"

sudo chown -R ${USER}:${USER} "${OUTDIR}"

pushd "${OUTDIR}"
pip uninstall -y "${OUTDIR}" || echo "Not installed"
pip install .
popd
