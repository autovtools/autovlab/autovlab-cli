# Stages:
#   builder - lint source, install w/deps
#   base    - Copy python lib from builder
#   debug   - Install debug tools on base

###############################################################################
#
###############################################################################
FROM autovlab/autovlab:pip as builder

USER autovlab

# Install pip dependencies first so the layers can be cached 
# We only need to reinstall dependencies if setup.py changed
COPY setup.py /home/autovlab/
RUN python ~/setup.py egg_info -e ~/ && \
    python -m pip install --user --no-warn-script-location --no-cache-dir -r ~/*.egg-info/requires.txt


# Copy in our source
COPY . /home/autovlab/src
WORKDIR /home/autovlab/src

# Fix permissions for autovlab
USER root
RUN chown autovlab:autovlab -R .

# Install the generated autovlab-api (submodule)
RUN cd autovlab-api && \
    pip install --no-cache-dir . &&\
    mkdir -p /opt/whl && \
    python setup.py bdist_wheel --dist-dir /opt/whl

# Lint + install + uninstall linter
# (Find the name of the python package dir by finding __init__.py)
RUN pip install --no-cache-dir . && \
    pip install --no-cache-dir pylint && \
    pylint -E "$(dirname $(find . -maxdepth 2 -name __init__.py))" && \
    python setup.py bdist_wheel --dist-dir /opt/whl && \
    pip uninstall -y pylint

###############################################################################
# prod / run image
###############################################################################
FROM python:slim as base

# Install fish
RUN apt update && apt install -y --no-install-recommends \
    fish && \
    rm -rf /var/lib/apt/lists/*

# Add non-root user and switch to it
RUN useradd -ms /usr/bin/fish autovlab
USER autovlab

# Copy the python install from builder (which includes all dependenices)
WORKDIR /home/autovlab
COPY --from=builder /home/autovlab/.local /home/autovlab/.local
COPY --from=builder /opt/whl/ /opt/whl/

USER root
RUN pip install /opt/whl/*.whl
USER autovlab

RUN autovlab-cli --install-completion fish

ENTRYPOINT ["/usr/bin/fish"]

###############################################################################
# debug: install IPython and default to IPython shell
###############################################################################
FROM base as debug

RUN python -m pip install --user --no-warn-script-location --no-cache-dir IPython || true
ENTRYPOINT ["/usr/bin/fish"]
