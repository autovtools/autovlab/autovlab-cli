#!/bin/bash
set -e
export SECRETS="/opt/secrets/"
export AUTOVLAB_CA_CRT="${SECRETS}/autovlab_ca.crt"

docker-compose build
docker-compose run --rm autovlab-cli
#docker-compose run --rm autovlab-cli python -m autovlab_cli --debug
